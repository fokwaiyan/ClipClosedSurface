//C++
#include <algorithm>

// For the rendering pipeline setup:
#include <vtkSmartPointer.h>
#include <vtkSTLReader.h>
#include <vtkSTLWriter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkProperty.h>
// For vtkBoxWidget2:
#include <vtkBoxWidget2.h>
#include "MyBoxWidget.h"
#include <vtkBoxRepresentation.h>
#include <vtkCommand.h>
#include <vtkTransform.h>
#include "MyBoxRepresentation.h"

//For clipping
#include <vtkClipPolyData.h>
#include <vtkBox.h>
#include <vtkClipClosedSurface.h>
#include <vtkPlaneCollection.h>
#include <vtkPlanes.h>

//For ray cast
#include <vtkOBBTree.h>
#include <vtkModifiedBSPTree.h>
#include <vtkKdTreePointLocator.h>
#include <vtkLineSource.h>
#include <vtkSphereSource.h>
#include <vtkBoundingBox.h>
#include <vtkCubeSource.h>
#include <vtkIdList.h>
#include <vtkIdTypeArray.h>
#include <vtkUnstructuredGrid.h>

//for intersecting surface
#include <vtkLine.h>
#include <vtkSelectEnclosedPoints.h>
#include <vtkImplicitPolyDataDistance.h>
#include <vtkIntersectionPolyDataFilter.h>
#include <vtkCleanPolyData.h>
#include <vtkPolyLineSortIDFilter.h>		//
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkGeometryFilter.h>
#include "vtkSurfaceEncloser.h"

//for creating faces
#include <vtkPolygon.h>
#include <vtkAppendPolyData.h>

//Callback
#include <vtkCallbackCommand.h>
//Save Mesh
#include <vtkTriangleFilter.h>

//Writing file
#include <vtkXMLPolyDataWriter.h>
#include "vtkMath.h"
#include "vtkCellArray.h"

//mouse action to adjust size of box
class vtkBoxCallback : public vtkCommand
{
public:
  static vtkBoxCallback *New()
  {
    return new vtkBoxCallback;
  }
  void SetIntersection(vtkSmartPointer<vtkPolyData> intersection)
  {
	  m_intersection = intersection;
  }

  void SetInputData(vtkSmartPointer<vtkPolyData> input)
  {
	  m_input = input;
  }
  void SetBoxWidget(vtkSmartPointer<MyBoxWidget> boxWidget)
  {
	  m_boxWidget = boxWidget;
  }
  void SetRenderer(vtkSmartPointer<vtkRenderer> renderer)
  {
	  m_renderer = renderer;
  }

  virtual void Execute( vtkObject *caller, unsigned long, void* )
  {
    vtkRenderWindowInteractor *iren =
		 static_cast<vtkRenderWindowInteractor*>(caller);

	 std::string key = iren->GetKeySym();

	 if (key == "Delete")
	 {
		// m_boxWidget->GetRepresentation()->PrintSelf(std::cout,);

		 vtkSmartPointer<vtkTransform> t = vtkSmartPointer<vtkTransform>::New();
		 vtkSmartPointer<vtkBox> implicitCube = vtkSmartPointer<vtkBox>::New();
		 vtkSmartPointer<vtkPolyData> boxPD = vtkSmartPointer<vtkPolyData>::New();
		 MyBoxRepresentation* boxRep = MyBoxRepresentation::SafeDownCast(m_boxWidget->GetRepresentation());
		 
		 boxRep->GetPolyData(boxPD);
		 boxRep->GetTransform(t);
		 t->Inverse();
		 implicitCube->SetBounds(m_input->GetBounds());
		 implicitCube->SetTransform(t);		 
		 		
		 //display box clip PD and  triangluate for intersection
		 vtkSmartPointer<vtkTriangleFilter> cubeTri = vtkSmartPointer<vtkTriangleFilter>::New();
		 cubeTri->SetInputData(boxPD);
		 cubeTri->Update();
		 vtkSmartPointer<vtkPolyDataMapper> cubeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		 cubeMapper->SetInputData(cubeTri->GetOutput());
		 cubeMapper->ScalarVisibilityOff();
		 vtkSmartPointer<vtkActor> cubeActor = vtkSmartPointer<vtkActor>::New();
		 cubeActor->SetMapper(cubeMapper);
		 cubeActor->GetProperty()->SetColor(1, 0, 0);
		 cubeActor->GetProperty()->SetOpacity(0.5);
		 m_renderer->AddActor(cubeActor);

		 //display input PD and  triangluate for intersection
		 vtkSmartPointer<vtkTriangleFilter> inputTri = vtkSmartPointer<vtkTriangleFilter>::New();
		 inputTri->SetInputData(m_input);
		 inputTri->Update();

		 //Intersection of box and input
		 vtkSmartPointer<vtkIntersectionPolyDataFilter> intersect = vtkSmartPointer<vtkIntersectionPolyDataFilter>::New();
		 intersect->SetInputData(0, inputTri->GetOutput());
		 intersect->SetInputData(1, cubeTri->GetOutput());
		 intersect->GetSplitFirstOutput();
		 intersect->Update();

		 vtkSmartPointer<vtkCleanPolyData> intersectClean = vtkSmartPointer<vtkCleanPolyData>::New();
		 intersectClean->SetInputData(intersect->GetOutput());
		 intersectClean->Update();
		 //clipping, later remove cells inside the clip box
		 vtkSmartPointer<vtkClipPolyData>  clipper = vtkSmartPointer<vtkClipPolyData>::New();
		 clipper->SetInputData(m_input);
		 clipper->SetClipFunction(implicitCube);
		 clipper->Update();

		 vtkSmartPointer<vtkClipPolyData>  clipper2 = vtkSmartPointer<vtkClipPolyData>::New();
		 clipper2->SetInputData(clipper->GetOutput());
		 clipper2->SetClipFunction(implicitCube);
		 clipper2->Update();

	
		 vtkSmartPointer<vtkPolyDataMapper> intersectionMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		 intersectionMapper->SetInputData(intersect->GetOutput(0));
		 intersectionMapper->ScalarVisibilityOff();
		 vtkSmartPointer<vtkActor> intersectionActor = vtkSmartPointer<vtkActor>::New();
		 intersectionActor->SetMapper(intersectionMapper);
		 intersectionActor->GetProperty()->SetColor(1, 0, 0);
		 intersectionActor->GetProperty()->SetLineWidth(5.0);
		 m_renderer->AddActor(intersectionActor);

		 vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
		 writer->SetFileName("D:/STFast/Project/data/Intersect-line-unsorted.vtp");
		 writer->SetInputData(intersect->GetOutput());
		 writer->Update();

		 vtkSmartPointer<vtkPolyDataConnectivityFilter> interConn = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
		 interConn->SetInputData(intersectClean->GetOutput());
		 interConn->SetExtractionModeToAllRegions();
		 interConn->Update();
		 int topNum = 0;		//find the total no. of points in top intersect line
		 int botNum = 0;        //find the total no. of points in bottom intersect line
		 std::cout << "No. of regions in unsorted intersections: " << interConn->GetNumberOfExtractedRegions()<<endl;
	     vtkPolyLineSortDIFilter* sorter = vtkPolyLineSortDIFilter::New();
		 vtkSmartPointer<vtkPolyData> sortedPD = vtkSmartPointer<vtkPolyData>::New();	
		 std::vector<vtkPolyData*> sortedPd;
		 if (interConn->GetNumberOfExtractedRegions() == 1) {
			 //sort the IDs in polydata 
			 sorter->SetInputData(intersectClean->GetOutput(0));
			 sorter->Update();
			 sortedPD->DeepCopy(sorter->GetOutput());
			 sortedPd.push_back(sortedPD);
		 }
		 else if (interConn->GetNumberOfExtractedRegions() >=2) {
			 vtkSmartPointer<vtkPolyDataConnectivityFilter> inter2Conn = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
			 for (int i=0; i< interConn->GetNumberOfExtractedRegions(); i++) {
				 vtkSmartPointer<vtkPolyDataConnectivityFilter> inter2Conn = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
				 inter2Conn->SetInputData(interConn->GetOutput());
				 inter2Conn->SetExtractionModeToSpecifiedRegions();
				 inter2Conn->AddSpecifiedRegion(i);
				 inter2Conn->Update();

				 vtkPolyLineSortDIFilter* sorter2 = vtkPolyLineSortDIFilter::New();
				 sorter2->SetInputData(inter2Conn->GetOutput());
				 sorter2->Update();
				
				 if (i ==0){
					 topNum = sorter2->GetOutput()->GetNumberOfPoints();
				 }
				 else if (i ==1){
					 botNum = sorter2->GetOutput()->GetNumberOfPoints();
				 }
				 
				 vtkSmartPointer<vtkAppendPolyData> appPD = vtkSmartPointer<vtkAppendPolyData>::New();
				 appPD->AddInputData(sorter2->GetOutput());
				 appPD->AddInputData(sortedPD);
				 appPD->Update();
				 sortedPD->DeepCopy(appPD->GetOutput());
				 sortedPd.push_back(sorter2->GetOutput());
			 }
		 }
		 
		 vtkSmartPointer<vtkXMLPolyDataWriter> writer2 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
		 writer2->SetFileName("D:/STFast/Project/data/Intersect-line-sorted.vtp");
		 writer2->SetInputData(sortedPD);
		 writer2->Update();

		 vtkSmartPointer<vtkPoints> linePoint = vtkSmartPointer<vtkPoints>::New();
		 vtkSmartPointer<vtkPoints> intersectedPoints = vtkSmartPointer<vtkPoints>::New();
		 vtkSmartPointer<vtkPolyData> pointPD = vtkSmartPointer<vtkPolyData>::New();
		 std::vector<int> intersectedIdList;
		 int point0 = 0;
		 int point1 = 4;
		 for (int i =0; i< 4; i++) //box has 8 edges, each time process a pair of edges along z direction
		 {
			 double p0[3];
			 double p1[3];
			 memcpy(p0, boxPD->GetPoint(point0), sizeof(p0));
			 memcpy(p1, boxPD->GetPoint(point1), sizeof(p1));

			 linePoint->InsertNextPoint(p0);
			 linePoint->InsertNextPoint(p1);
			 std::vector<int> tempIdList;	 
			 vtkSmartPointer<vtkPoints> tempIntersectedPoints = vtkSmartPointer<vtkPoints>::New();

			 tempIdList= this->_FindClosestPointtoLine(sortedPD, p0, p1, tempIntersectedPoints);
			 			 
			 if (tempIdList.size() > 0 && tempIntersectedPoints->GetNumberOfPoints() > 0){
				 for (int j = 0; j < tempIdList.size(); j++) {
					 intersectedIdList.push_back(tempIdList.at(j));
					 intersectedPoints->InsertNextPoint(tempIntersectedPoints->GetPoint(j));
				 }
				std::cout<<intersectedPoints->GetNumberOfPoints() << endl;
			 }
     		//std::sort(intersectedIdList.begin(), intersectedIdList.end());
			std::cout << "intersected id list: " ;
			for (int a = 0; a< intersectedIdList.size(); a++) {
				std::cout << intersectedIdList.at(a)<< " ";
			}
			std::cout << endl;
			 point0++;
			 point1++;
		 }

		 pointPD->SetPoints(intersectedPoints);
		 std::cout<<"POintPd: "<< pointPD->GetNumberOfPoints()<<endl;
		 for (int k = 0; k < pointPD->GetNumberOfPoints(); k++) {
			 std::cout << pointPD->GetPoint(k)[0] << "," << pointPD->GetPoint(k)[1] << "," << pointPD->GetPoint(k)[2] << endl;
		 }
		 vtkSmartPointer<vtkXMLPolyDataWriter> intersectWriter = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
		 intersectWriter->SetFileName("D:/STFast/Project/data/Intersect-Points.vtp");
		 intersectWriter->SetInputData(pointPD);
		 intersectWriter->Write();		

		 vtkSmartPointer<vtkPoints> facePoints1 = vtkSmartPointer<vtkPoints>::New();
		 vtkSmartPointer<vtkPoints> facePoints2 = vtkSmartPointer<vtkPoints>::New();
		 vtkSmartPointer<vtkPoints> facePoints3 = vtkSmartPointer<vtkPoints>::New();
		 vtkSmartPointer<vtkPoints> facePoints4 = vtkSmartPointer<vtkPoints>::New();
		 vtkSmartPointer<vtkPolygon> poly = vtkSmartPointer<vtkPolygon>::New();
		 vtkSmartPointer<vtkPolygon> poly2 = vtkSmartPointer<vtkPolygon>::New();
		 vtkSmartPointer<vtkPolygon> poly3 = vtkSmartPointer<vtkPolygon>::New();
		 vtkSmartPointer<vtkPolygon> poly4 = vtkSmartPointer<vtkPolygon>::New();
		 vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();
		 vtkSmartPointer<vtkCellArray> cellArray2 = vtkSmartPointer<vtkCellArray>::New();
		 vtkSmartPointer<vtkCellArray> cellArray3 = vtkSmartPointer<vtkCellArray>::New();
		 vtkSmartPointer<vtkCellArray> cellArray4 = vtkSmartPointer<vtkCellArray>::New();
		 vtkSmartPointer<vtkPolyData> face1 = vtkSmartPointer<vtkPolyData>::New();
		 vtkSmartPointer<vtkPolyData> face2 = vtkSmartPointer<vtkPolyData>::New();
		 vtkSmartPointer<vtkPolyData> face3 = vtkSmartPointer<vtkPolyData>::New();
		 vtkSmartPointer<vtkPolyData> face4 = vtkSmartPointer<vtkPolyData>::New();
		 // Create Face
		 if (intersectedIdList.size() == 4) //create 3 faces
		 {
			 std::cout << "Create 3 faces." << endl;
			 std::sort(intersectedIdList.begin(), intersectedIdList.end());
			 //create the center face
			 for (int i = intersectedIdList.at(0); i <= intersectedIdList.at(1); i++) {
				 facePoints1->InsertNextPoint(sortedPD->GetPoint(i));
				 poly->GetPointIds()->InsertNextId(facePoints1->GetNumberOfPoints() - 1);
			 }
			 for (int i = intersectedIdList.at(2); i <= intersectedIdList.at(3); i++) {
				 facePoints1->InsertNextPoint(sortedPD->GetPoint(i));
				 poly->GetPointIds()->InsertNextId(facePoints1->GetNumberOfPoints() - 1);
			 }
			 poly->GetPointIds()->InsertNextId(0);			 
			 cellArray->InsertNextCell(poly);
			 face1->SetPoints(facePoints1); //one face
			 face1->SetPolys(cellArray);

			 //create side face
			 for (int i = intersectedIdList.at(1); i <= intersectedIdList.at(2); i++) {
				 facePoints2->InsertNextPoint(sortedPD->GetPoint(i));
				 poly2->GetPointIds()->InsertNextId(facePoints2->GetNumberOfPoints() - 1);
			 }
			 poly2->GetPointIds()->InsertNextId(facePoints2->GetNumberOfPoints() - 1);
			 cellArray2->InsertNextCell(poly2);
			 face2->SetPoints(facePoints2); //one face
			 face2->SetPolys(cellArray2);
			 
			 //create 2nd side face
			 for (int i = intersectedIdList.at(3); i < sortedPD->GetNumberOfPoints(); i++) {
				 facePoints3->InsertNextPoint(sortedPD->GetPoint(i));
				 poly3->GetPointIds()->InsertNextId(facePoints3->GetNumberOfPoints() - 1);
			 }
			 for (int i = 0; i <= intersectedIdList.at(0); i++)
			 {
				 facePoints3->InsertNextPoint(sortedPD->GetPoint(i));
				 poly3->GetPointIds()->InsertNextId(facePoints3->GetNumberOfPoints() - 1);
			 }
			 poly3->GetPointIds()->InsertNextId(facePoints3->GetNumberOfPoints() - 1);
			 cellArray3->InsertNextCell(poly3);
			 face3->SetPoints(facePoints3); //one face
			 face3->SetPolys(cellArray3);
		 }
		 else if (intersectedIdList.size() ==8) //create 4 faces
		 {
			 std::cout << "Create 4 faces. " << endl;
			 vtkSmartPointer<vtkPolyData> spoly1 = vtkSmartPointer<vtkPolyData>::New();
			 spoly1->DeepCopy(sortedPd.at(0));
			 spoly1->Print(std::cout);
			 vtkSmartPointer<vtkPolyData> spoly2 = vtkSmartPointer<vtkPolyData>::New();
			 spoly2->DeepCopy(sortedPd.at(1));
			 spoly2->Print(std::cout);
			 //vtkSmartPointer<vtkTriangleFilter> trianglePoly1 = vtkSmartPointer<vtkTriangleFilter>::New();
			 //trianglePoly1->SetInputData(spoly1);
			 //trianglePoly1->Update();
			 //spoly1->DeepCopy(trianglePoly1->GetOutput());
			 //vtkSmartPointer<vtkTriangleFilter> trianglePoly2 = vtkSmartPointer<vtkTriangleFilter>::New();
			 //trianglePoly2->SetInputData(spoly2);
			 //trianglePoly2->Update();
			 //spoly2->DeepCopy(trianglePoly2->GetOutput());

			 vtkSmartPointer<vtkCenterOfMass> comfilter = vtkSmartPointer<vtkCenterOfMass>::New();
			 comfilter->SetInputData(spoly1);
			 comfilter->Update();
			 double* com1 = comfilter->GetCenter();
			 double* norm1 = _FindNormal(spoly1, com1);
			 
			 vtkSmartPointer<vtkCenterOfMass> comfilter2 = vtkSmartPointer<vtkCenterOfMass>::New();
			 comfilter2->SetInputData(spoly2);
			 comfilter2->Update();
			 double* com2 = comfilter2->GetCenter();
			 double* norm2 = _FindNormal(spoly2, com2);

			 double dotProduct = vtkMath::Dot(norm1, norm2);
			 std::cout << "norm1: " << norm1[0] << " " << norm1[1] << " " << norm1[2] << endl;
			 std::cout << "norm2: " << norm2[0] << " " << norm2[1] << " " << norm2[2] << endl;
			 std::cout << "Dot product: " << dotProduct << endl;
			 int n = spoly2->GetNumberOfPoints();
			 std::cout << "spoly2 num of pts: "<< n << endl;
			 if (dotProduct < 0)
			 {
				 //resort the polyline
				 vtkSmartPointer<vtkPolyData> reOrderPD = vtkSmartPointer<vtkPolyData>::New();
				 vtkSmartPointer<vtkPoints> xPoint = vtkSmartPointer<vtkPoints>::New();
				 for (int x= n-1; x >= 0 ; x--) 
				 {
					 xPoint->InsertNextPoint(spoly2->GetPoint(x));
				 }
					spoly2->SetPoints(xPoint);
			 }
			 
			 vtkSmartPointer<vtkXMLPolyDataWriter> triangle1 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
			 triangle1->SetFileName("D:/STFast/Project/data/before1.vtp");
			 triangle1->SetInputData(spoly1);
			 triangle1->Update();
			 triangle1->Write();

			 vtkSmartPointer<vtkXMLPolyDataWriter> triangle2 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
			 triangle2->SetFileName("D:/STFast/Project/data/before2.vtp");
			 triangle2->SetInputData(spoly2);
			 triangle2->Update();
			 triangle2->Write();


			 SurfaceEncloser* surfaceEncloser = new SurfaceEncloser;
			 surfaceEncloser->SetInput(spoly1,spoly2);
			 surfaceEncloser->SetEdgesAreLoops(true);
			 surfaceEncloser->Update();
			 face4->DeepCopy(surfaceEncloser->GetOutput());
		 }


		 ////////////// Print face
		 vtkSmartPointer<vtkXMLPolyDataWriter> facewrit1 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
		 facewrit1->SetFileName("D:/STFast/Project/data/face11.vtp");
		 facewrit1->SetInputData(face1);
		 facewrit1->Update();
		 facewrit1->Write();
		 vtkSmartPointer<vtkXMLPolyDataWriter> facewrit2 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
		 facewrit2->SetFileName("D:/STFast/Project/data/face22.vtp");
		 facewrit2->SetInputData(face2);
		 facewrit2->Update();
		 facewrit2->Write();
		 vtkSmartPointer<vtkXMLPolyDataWriter> facewrit3 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
		 facewrit3->SetFileName("D:/STFast/Project/data/face33.vtp");
		 facewrit3->SetInputData(face3);
		 facewrit3->Update();
		 facewrit3->Write();
		 vtkSmartPointer<vtkXMLPolyDataWriter> facewrit4 = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
		 facewrit4->SetFileName("D:/STFast/Project/data/face44.vtp");
		 facewrit4->SetInputData(face4);
		 facewrit4->Update();
		 facewrit4->Write();
		 vtkSmartPointer<vtkCleanPolyData> faceClean = vtkSmartPointer<vtkCleanPolyData>::New();
		 faceClean->SetInputData(face1);
		 faceClean->Update();
		 vtkSmartPointer<vtkCleanPolyData> face2Clean = vtkSmartPointer<vtkCleanPolyData>::New();
		 face2Clean->SetInputData(face2);
		 face2Clean->Update();
		 vtkSmartPointer<vtkCleanPolyData> face3Clean = vtkSmartPointer<vtkCleanPolyData>::New();
		 face3Clean->SetInputData(face3);
		 face3Clean->Update();
		 vtkSmartPointer<vtkTriangleFilter> faceTri = vtkSmartPointer<vtkTriangleFilter>::New();
		 faceTri->SetInputData(faceClean->GetOutput());
		 faceTri->Update();
		 vtkSmartPointer<vtkTriangleFilter> faceTri2 = vtkSmartPointer<vtkTriangleFilter>::New();
		 faceTri2->SetInputData(face2Clean->GetOutput());
		 faceTri2->Update();
		 vtkSmartPointer<vtkTriangleFilter> faceTri3 = vtkSmartPointer<vtkTriangleFilter>::New();
		 faceTri3->SetInputData(face3Clean->GetOutput());
		 faceTri3->Update();
		 vtkSmartPointer<vtkPolyDataMapper> faceMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		 faceMapper->SetInputData(faceTri->GetOutput());
		 vtkSmartPointer<vtkActor> faceActor = vtkSmartPointer<vtkActor>::New();
		 faceActor->SetMapper(faceMapper);
		 m_renderer->AddActor(faceActor);
		 vtkSmartPointer<vtkPolyDataMapper> faceMapper2 = vtkSmartPointer<vtkPolyDataMapper>::New();
		 faceMapper2->SetInputData(faceTri2->GetOutput());
		 vtkSmartPointer<vtkActor> faceActor2 = vtkSmartPointer<vtkActor>::New();
		 faceActor2->SetMapper(faceMapper2);
		 m_renderer->AddActor(faceActor2);
		 vtkSmartPointer<vtkPolyDataMapper> faceMapper3 = vtkSmartPointer<vtkPolyDataMapper>::New();
		 faceMapper3->SetInputData(faceTri3->GetOutput());
		 vtkSmartPointer<vtkActor> faceActor3 = vtkSmartPointer<vtkActor>::New();
		 faceActor3->SetMapper(faceMapper3);
		 m_renderer->AddActor(faceActor3);
		 vtkSmartPointer<vtkPolyDataMapper> faceMapper4 = vtkSmartPointer<vtkPolyDataMapper>::New();
		 faceMapper4->SetInputData(face4);
		 vtkSmartPointer<vtkActor> faceActor4 = vtkSmartPointer<vtkActor>::New();
		 faceActor4->SetMapper(faceMapper4);
		 //m_renderer->AddActor(faceActor4);
		 //////////////



		 m_input->DeepCopy(clipper->GetOutput()); 
		 vtkSmartPointer<vtkAppendPolyData> appender = vtkSmartPointer<vtkAppendPolyData>::New();
		 appender->AddInputData(m_input);
		 appender->AddInputData(faceTri->GetOutput());
		 appender->AddInputData(faceTri2->GetOutput());
		 appender->AddInputData(faceTri3->GetOutput());
		 appender->AddInputData(face4);
		 appender->Update();
		 m_input->DeepCopy(appender->GetOutput());
		 
		 m_renderer->AddActor(intersectionActor);

		 iren->Render();
	 }
	 if (key == "Return")
	 {
		 std::cout << "Finished.";
		 m_boxWidget->Off();
		 iren->Render();
		 
		 vtkSmartPointer<vtkTriangleFilter> triangle = vtkSmartPointer<vtkTriangleFilter>::New();
		 triangle->SetInputData(m_input);
		 triangle->Update();

		 std::string filename = "D:/STFast/Project/data/A3.stl";
		 vtkSmartPointer<vtkSTLWriter> writer = vtkSmartPointer<vtkSTLWriter>::New();
		 writer->SetFileName(filename.c_str());
		 writer->SetInputData(triangle->GetOutput());
		 writer->Write();
	 }
  }
 
  double * _FindNormal(vtkSmartPointer<vtkPolyData> inPD, double* com)
  {
	  double* outNormal = (double*)malloc(sizeof(double) * 3);
	  outNormal[0] = 0;
	  outNormal[1] = 0;
	  outNormal[2] = 0;

	  for (int i = 0; i < inPD->GetNumberOfPoints(); i++)
	  {
		  double* crossProduct = (double*)malloc(sizeof(double) * 3);
		  double vect1[3], vect2[3];
		  vtkMath::Subtract(inPD->GetPoint(i), com, vect1);
		  vtkMath::Subtract(inPD->GetPoint((i + 1) % inPD->GetNumberOfPoints()), com, vect2);
		  vtkMath::Cross(vect2, vect1, crossProduct);
		  vtkMath::Add(outNormal, crossProduct, outNormal);

		  free(crossProduct);
	  }
	  vtkMath::Normalize(outNormal);
	  return outNormal;
  }

  void _MarkCellsInsidePolygon(vtkSmartPointer<vtkPolyData> inPD, vtkSmartPointer<vtkPolyData> clipperPD, vtkIdList *outList)
  {
	  vtkIdList* tmpList = vtkIdList::New();
	  vtkSmartPointer<vtkSelectEnclosedPoints> selector = vtkSmartPointer<vtkSelectEnclosedPoints>::New();
	  selector->SetInputData(inPD);
	  selector->SetSurfaceData(clipperPD);
	  selector->Update();

	  vtkSmartPointer<vtkImplicitPolyDataDistance> ipdd = vtkSmartPointer<vtkImplicitPolyDataDistance>::New();
	  ipdd->SetInput(clipperPD);
	  ipdd->SetTolerance(0);

	  // Can be optimize to locate possible cells first
	  for (int i = 0; i < inPD->GetNumberOfCells(); i++)
	  {
		  vtkCell* l_cell = inPD->GetCell(i);
		  double *l_cellCoord = (double*)malloc(sizeof(double) * 3);
		  this->_GetCellLocation(inPD, l_cell, l_cellCoord);
		  double l_inpolygon = ipdd->EvaluateFunction(l_cellCoord);
		  if (l_inpolygon <= 0 && l_cell->GetNumberOfPoints() >= 3) {
			  tmpList->InsertUniqueId(i);
			  //cout << "Marking " << i << endl;
		  }
		  std::free(l_cellCoord);
	  }
	  outList->DeepCopy(tmpList);
  }

  void _GetCellLocation(vtkSmartPointer<vtkPolyData> inPD, vtkCell* inCell, double *outPt)
  {
	  double outtmp[3] = { 0,0,0 };
	  int l_looper = 0;
	  for (int i = 0; i < inCell->GetNumberOfPoints(); i++) {
		  outtmp[0] += inPD->GetPoint(inCell->GetPointId(i))[0];
		  outtmp[1] += inPD->GetPoint(inCell->GetPointId(i))[1];
		  outtmp[2] += inPD->GetPoint(inCell->GetPointId(i))[2];
	  }
	  vtkMath::MultiplyScalar(outtmp, 1. / inCell->GetNumberOfPoints());
	  memcpy(outPt, outtmp, sizeof(double) * 3);
  }

  std::vector<int> _FindClosestPointtoLine(vtkSmartPointer<vtkPolyData> polyline, double p0[3], double p1[3], vtkSmartPointer<vtkPoints> intersectPoints)
  {
	  std::vector<int> intersectedIdList;
	  std::cout << "p0: " << p0[0] << " " << p0[1] << " " << p0[2] << " " << endl<<  "p1: " << p1[0] << " " << p1[1] << " " << p1[2] << " " << endl;
	  double n[3];
	  vtkMath::Subtract(p0, p1, n);
	  vtkMath::Normalize(n);
	  // exceptional case: check if n xyz are zeros
	  
	  //find the cell upper to p0
	  vtkSmartPointer<vtkPoints> intersectP0 = vtkSmartPointer<vtkPoints>::New();
	  std::vector<double> varianceP0;
	  std::vector<int> pointIdP0;

	  typedef std::pair<int, double> IdVar;
	  std::list<IdVar*> idVar0;

	  for (int i = 0; i < polyline->GetNumberOfPoints(); i++)
	  {
		  //A is the query point
		  double A1 = 0;
		  double A2 = 0;
		  double A3 = 0;
		  double p[3];
		  memcpy(p, polyline->GetPoint(i), sizeof(p));
		  A1 = (p[0] - p0[0]) / n[0];		//x
		  A2 = (p[1] - p0[1]) / n[1];		//y
		  A3 = (p[2] - p0[2]) / n[2];		//z
			
		  double mean = (A1 + A2 + A3) / 3;
		  double var=0;
		  if (abs(mean) < 1)
			var = pow((A1 - mean), 2) + pow((A2 - mean), 2) + pow(A3 - mean, 2);
		  else
			var = (pow((A1 - mean), 2) + pow((A2 - mean), 2) + pow(A3 - mean, 2))/pow(mean,2);
		 		  
		  double thres = 0.0001;
		  if (abs(var) > thres){
			  //reject;
		  }
		  else {
			  // push in some list
			  intersectP0->InsertNextPoint(p);
			  std::cout << "Intersect P0 variance: " << var << endl << "Point " << i <<": "<< p[0] << " " << p[1] << " " << p[2] << " " << endl;
			  pointIdP0.push_back(i);
			  varianceP0.push_back(var);
		  }
	  }
	  //find nearest with p1
	  vtkSmartPointer<vtkPoints> intersectP1 = vtkSmartPointer<vtkPoints>::New();
	  std::vector<double> varianceP1;
	  std::vector<int>	 pointIdP1;
	  for (int i = 0; i < polyline->GetNumberOfPoints(); i++)
	  {
		  double A1 = 0;
		  double A2 = 0;
		  double A3 = 0;
		  double* p = polyline->GetPoint(i);
		  A1 = (p[0] - p1[0]) / n[0];		//x
		  A2 = (p[1] - p1[1]) / n[1];		//y
		  A3 = (p[2] - p1[2]) / n[2];		//z

		  double mean = (A1 + A2 + A3) / 3;
		  double var;
		  if (abs(mean) < 1)
			  var = pow((A1 - mean), 2) + pow((A2 - mean), 2) + pow(A3 - mean, 2);
		  else
			  var = pow((A1 - mean), 2) + pow((A2 - mean), 2) + pow(A3 - mean, 2) / pow(mean,2);

		  double thres = 0.0001;
		  if (abs(var) > thres) {
			  //reject;
		  }
		  else {
			  // push in some list
			  intersectP1->InsertNextPoint(p);
			  std::cout << "Intersect P1  variance: " << var << endl<< "Point "<<i <<": "<<p[0] << " " << p[1] << " " << p[2] << " " << endl;
			  pointIdP1.push_back(i);
			  varianceP1.push_back(var);
		  }
	  }
	  // find the min var and respective pt
	  std::vector<double>::iterator resultP0;
	  std::vector<double>::iterator resultP1;
	  int d0 = 0;
	  int d1 = 0;
	  if (varianceP1.size() != 0 && varianceP0.size() != 0)
	  {
		  resultP0 = std::min_element(varianceP0.begin(), varianceP0.end());
		  resultP1 = std::min_element(varianceP1.begin(), varianceP1.end());
		  d0 = std::distance(varianceP0.begin(), resultP0);
		  d1 = std::distance(varianceP1.begin(), resultP1);
		  std::cout << "min var of P0: " << d0 << endl << "min var of P1: " << d1 << endl;

		  if (pointIdP0.at(d0) == pointIdP1.at(d1)) {
			  //get second min in resultP1
			  pointIdP1.erase(std::remove(pointIdP1.begin(), pointIdP1.end(), pointIdP0.at(d0)), pointIdP1.end());
			  varianceP1.erase(varianceP1.begin()+d1);
			  
			  std::vector<double>::iterator resultP11 = std::min_element(varianceP1.begin(), varianceP1.end());
			  int d11 = std::distance(varianceP1.begin(), resultP11);
			  intersectedIdList.push_back(pointIdP0.at(d0));
			  intersectedIdList.push_back(pointIdP1.at(d11));
			  intersectPoints->InsertNextPoint(intersectP0->GetPoint(d0));
			  intersectPoints->InsertNextPoint(intersectP1->GetPoint(d11));
		  }
		  else  {
			  intersectedIdList.push_back(pointIdP0.at(d0));
			  intersectedIdList.push_back(pointIdP1.at(d1));
			  intersectPoints->InsertNextPoint(intersectP0->GetPoint(d0));
			  intersectPoints->InsertNextPoint(intersectP1->GetPoint(d1));
		  }
	  } 
	  return intersectedIdList;
  }

  
//// for two polylines
// std::vector<pointIdPair*> _FindClosestPointtoLine(std::vector<vtkSmartPointer<vtkPolyData>> input, double p0[3], double p1[3], vtkSmartPointer<vtkPoints> intersectPoints)
//  {
//	  for (int z=0; z< input.size(); z++)
//	  {
//
//	  }
//	  std::vector<pointIdPair*> intersectedIdList;
//	  std::cout << "p0: " << p0[0] << " " << p0[1] << " " << p0[2] << " " << endl << "p1: " << p1[0] << " " << p1[1] << " " << p1[2] << " " << endl;
//	  double n[3];
//	  vtkMath::Subtract(p0, p1, n);
//	  vtkMath::Normalize(n);
//	  // exceptional case: check if n xyz are zeros
//
//	  //find the cell upper to p0
//	  vtkSmartPointer<vtkPoints> intersectP0 = vtkSmartPointer<vtkPoints>::New();
//	  std::vector<double> varianceP0;
//	  std::vector<int> pointIdP0;
//
//	  typedef std::pair<int, double> IdVar;
//	  std::list<IdVar*> idVar0;
//
//	  for (int i = 0; i < polyline->GetNumberOfPoints(); i++)
//	  {
//		  //A is the query point
//		  double A1 = 0;
//		  double A2 = 0;
//		  double A3 = 0;
//		  double p[3];
//		  memcpy(p, polyline->GetPoint(i), sizeof(p));
//		  A1 = (p[0] - p0[0]) / n[0];		//x
//		  A2 = (p[1] - p0[1]) / n[1];		//y
//		  A3 = (p[2] - p0[2]) / n[2];		//z
//
//		  double mean = (A1 + A2 + A3) / 3;
//		  double var = 0;
//		  if (abs(mean) < 1)
//			  var = pow((A1 - mean), 2) + pow((A2 - mean), 2) + pow(A3 - mean, 2);
//		  else
//			  var = (pow((A1 - mean), 2) + pow((A2 - mean), 2) + pow(A3 - mean, 2)) / pow(mean, 2);
//
//		  double thres = 0.0001;
//		  if (abs(var) > thres) {
//			  //reject;
//		  }
//		  else {
//			  // push in some list
//			  intersectP0->InsertNextPoint(p);
//			  std::cout << "Intersect P0 variance: " << var << endl << "Point " << i << ": " << p[0] << " " << p[1] << " " << p[2] << " " << endl;
//			  pointIdP0.push_back(i);
//			  varianceP0.push_back(var);
//		  }
//	  }
//	  //find nearest with p1
//	  vtkSmartPointer<vtkPoints> intersectP1 = vtkSmartPointer<vtkPoints>::New();
//	  std::vector<double> varianceP1;
//	  std::vector<int>	 pointIdP1;
//	  for (int i = 0; i < polyline->GetNumberOfPoints(); i++)
//	  {
//		  double A1 = 0;
//		  double A2 = 0;
//		  double A3 = 0;
//		  double* p = polyline->GetPoint(i);
//		  A1 = (p[0] - p1[0]) / n[0];		//x
//		  A2 = (p[1] - p1[1]) / n[1];		//y
//		  A3 = (p[2] - p1[2]) / n[2];		//z
//
//		  double mean = (A1 + A2 + A3) / 3;
//		  double var;
//		  if (abs(mean) < 1)
//			  var = pow((A1 - mean), 2) + pow((A2 - mean), 2) + pow(A3 - mean, 2);
//		  else
//			  var = pow((A1 - mean), 2) + pow((A2 - mean), 2) + pow(A3 - mean, 2) / pow(mean, 2);
//
//		  double thres = 0.0001;
//		  if (abs(var) > thres) {
//			  //reject;
//		  }
//		  else {
//			  // push in some list
//			  intersectP1->InsertNextPoint(p);
//			  std::cout << "Intersect P1  variance: " << var << endl << "Point " << i << ": " << p[0] << " " << p[1] << " " << p[2] << " " << endl;
//			  pointIdP1.push_back(i);
//			  varianceP1.push_back(var);
//		  }
//	  }
//	  // find the min var and respective pt
//	  std::vector<double>::iterator resultP0;
//	  std::vector<double>::iterator resultP1;
//	  int d0 = 0;
//	  int d1 = 0;
//	  if (varianceP1.size() != 0 && varianceP0.size() != 0)
//	  {
//		  resultP0 = std::min_element(varianceP0.begin(), varianceP0.end());
//		  resultP1 = std::min_element(varianceP1.begin(), varianceP1.end());
//		  d0 = std::distance(varianceP0.begin(), resultP0);
//		  d1 = std::distance(varianceP1.begin(), resultP1);
//		  std::cout << "min var of P0: " << d0 << endl << "min var of P1: " << d1 << endl;
//
//		  if (pointIdP0.at(d0) == pointIdP1.at(d1)) {
//			  //get second min in resultP1
//			  pointIdP1.erase(std::remove(pointIdP1.begin(), pointIdP1.end(), pointIdP0.at(d0)), pointIdP1.end());
//			  varianceP1.erase(varianceP1.begin() + d1);
//
//			  std::vector<double>::iterator resultP11 = std::min_element(varianceP1.begin(), varianceP1.end());
//			  int d11 = std::distance(varianceP1.begin(), resultP11);
//			  intersectedIdList.push_back(pointIdP0.at(d0));
//			  intersectedIdList.push_back(pointIdP1.at(d11));
//			  intersectPoints->InsertNextPoint(intersectP0->GetPoint(d0));
//			  intersectPoints->InsertNextPoint(intersectP1->GetPoint(d11));
//		  }
//		  else {
//			  intersectedIdList.push_back(pointIdP0.at(d0));
//			  intersectedIdList.push_back(pointIdP1.at(d1));
//			  intersectPoints->InsertNextPoint(intersectP0->GetPoint(d0));
//			  intersectPoints->InsertNextPoint(intersectP1->GetPoint(d1));
//		  }
//	  }
//	  return intersectedIdList;
//  }

  vtkBoxCallback(){}
private:
	vtkSmartPointer<vtkPolyData>	m_input;
	vtkSmartPointer<vtkPolyData>	m_intersection;
	vtkSmartPointer<MyBoxWidget>	m_boxWidget;
	vtkSmartPointer<vtkRenderer>	m_renderer;

	typedef std::pair<int, int> pointIdPair;
	
};


int main( int vtkNotUsed( argc ), char* vtkNotUsed( argv )[] )
{
  vtkSmartPointer<vtkPolyData> input = vtkSmartPointer<vtkPolyData>::New();
  vtkSmartPointer<vtkPolyData> input2 = vtkSmartPointer<vtkPolyData>::New();
  std::string inputFilename = "D:/STFast/Project/data/Cast-Vextrude-15.stl";

  vtkSmartPointer<vtkSTLReader > stlreader = vtkSmartPointer<vtkSTLReader >::New();
  stlreader->SetFileName(inputFilename.c_str());
  stlreader->Update();
  input->ShallowCopy((vtkDataObject*)stlreader->GetOutput());

  vtkSmartPointer<vtkSTLReader > stlreader2 = vtkSmartPointer<vtkSTLReader >::New();
  stlreader2->SetFileName("D:/STFast/CHEN LI_01.stl");
  stlreader2->Update();
  input2->ShallowCopy((vtkDataObject*)stlreader2->GetOutput());

  vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputData(input);

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper( mapper);
  actor->SetPosition(0, 0, 0);

  vtkSmartPointer<vtkPolyDataMapper> mapper2 = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper2->SetInputData(input2);

  vtkSmartPointer<vtkActor> actor2 = vtkSmartPointer<vtkActor>::New();
  //actor2->GetProperty()->SetOpacity(0.5);
  actor2->SetMapper(mapper2);
  
  vtkSmartPointer<vtkPolyData> intersectPD = vtkSmartPointer<vtkPolyData>::New();
  
  vtkSmartPointer<vtkPolyDataMapper> intersectionMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  intersectionMapper->SetInputData(intersectPD);
  intersectionMapper->ScalarVisibilityOff();

  vtkSmartPointer<vtkActor> intersectionActor = vtkSmartPointer<vtkActor>::New();
  intersectionActor->SetMapper(intersectionMapper);

  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor(actor);
  renderer->AddActor(intersectionActor);
  renderer->ResetCamera();				// Reposition camera so the whole scene is visible

  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer( renderer );
   
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow( renderWindow );

  // Use the "trackball camera" interactor style, rather than the default "joystick camera"
  vtkSmartPointer<vtkInteractorStyleTrackballCamera> style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
  renderWindowInteractor->SetInteractorStyle( style );

  vtkSmartPointer<MyBoxRepresentation> boxRep = vtkSmartPointer<MyBoxRepresentation>::New();
 
  vtkSmartPointer<MyBoxWidget> boxWidget = vtkSmartPointer<MyBoxWidget>::New();
  boxWidget->SetInteractor( renderWindowInteractor );
  boxWidget->SetRepresentation(boxRep);
  boxWidget->GetRepresentation()->SetPlaceFactor( 1 );				// Default is 0.5	//vtkBoxWidget2
  boxWidget->GetRepresentation()->PlaceWidget(actor->GetBounds());

  //boxWidget->GetHandleProperty()->SetColor(0, 1, 0);
  
  //boxWidget->GetRepresentation()->SetPlaceFactor( 1 );				// Default is 0.5	//vtkBoxWidget2
  //boxWidget->GetRepresentation()->PlaceWidget(actor->GetBounds());
  
  //boxWidget->GetRepresentation()->PlaceWidget()
  
  // Set up a callback for the interactor to call so we can manipulate the actor
  vtkSmartPointer<vtkBoxCallback> boxCallback = vtkSmartPointer<vtkBoxCallback>::New();
  boxCallback->SetInputData(input);
  boxCallback->SetBoxWidget(boxWidget);
  boxCallback->SetRenderer(renderer);
  renderWindowInteractor->AddObserver(vtkCommand::KeyPressEvent, boxCallback);
  
  boxWidget->On();

  renderWindowInteractor->Start();

  return EXIT_SUCCESS;

}



#include "vtkPolyLineSortIDFilter.h"

#include "vtkCellIterator.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include "vtkPoints.h"
#include "vtkCell.h"
#include "vtkCellArray.h"
#include "vtkLine.h"

vtkStandardNewMacro(vtkPolyLineSortDIFilter);

vtkPolyLineSortDIFilter::vtkPolyLineSortDIFilter()
{
	this->StartID = 0; // default value
}

vtkPolyLineSortDIFilter::~vtkPolyLineSortDIFilter()
{
	this->sortedEdges.clear();
	this->edges.clear();
}

int vtkPolyLineSortDIFilter::RequestData(vtkInformation *vtkNotUsed(request), vtkInformationVector **inputVector, vtkInformationVector *outputVector)
{
	vtkPolyData*	input = vtkPolyData::GetData(inputVector[0], 0);
	vtkPolyData*	output = vtkPolyData::GetData(outputVector, 0);

	/// Clean all lists
	edges.clear();
	sortedEdges.clear();

	/// Push all edges into a list
	vtkCellIterator* celliter = input->NewCellIterator();
	celliter->InitTraversal();

	while (!celliter->IsDoneWithTraversal())
	{
		if (celliter->GetNumberOfPoints() != 2)
		{
			vtkIdType cellid = celliter->GetCellId();
			vtkErrorMacro("Cell %i has %i points, only two points cells can be processed!"/*, cellid, celliter->GetNumberOfPoints()*/);
			return 0;
		}

		EdgePair* l_pair = new EdgePair;
		l_pair->first = celliter->GetPointIds()->GetId(0);
		l_pair->second = celliter->GetPointIds()->GetId(1);

		edges.push_back(l_pair);
		celliter->GoToNextCell();
	}

	/// Build sorted list
	if (!this->BuildSortedEdges())
		vtkWarningMacro("Build sorted list failed!");

	/// Reconstruct list
	vtkSmartPointer<vtkPoints> pts = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkLine> polylinecell = vtkSmartPointer<vtkLine>::New();
	for (std::list<EdgePair*>::iterator iter = sortedEdges.begin(); iter != sortedEdges.end(); iter++)
	{
		// insert both points for the first edge
		if (pts->GetNumberOfPoints() == 0)
		{
			pts->InsertNextPoint(input->GetPoint((*iter)->first));
			pts->InsertNextPoint(input->GetPoint((*iter)->second));
			polylinecell->GetPointIds()->InsertNextId(0);
			polylinecell->GetPointIds()->InsertNextId(1);
		}
		else {
			std::list<EdgePair*>::iterator start = sortedEdges.begin();
			// if this is a loop
			if ((*iter)->second == (*start)->first)
				polylinecell->GetPointIds()->InsertNextId(0);
			else
			{
				pts->InsertNextPoint(input->GetPoint((*iter)->second));
				polylinecell->GetPointIds()->InsertNextId(pts->GetNumberOfPoints() - 1);
			}
		}
	}
	output->SetPoints(pts);
	output->SetLines(vtkSmartPointer<vtkCellArray>::New());
	output->GetLines()->InsertNextCell(polylinecell);
}

int vtkPolyLineSortDIFilter::BuildSortedEdges()
{
	/// find an edge where first = StartID
	EdgePair* firstEdge = NULL;
	while(!firstEdge)
	{
		for (std::list<EdgePair*>::iterator iter = edges.begin(); iter != edges.end(); iter++)
		{
			if ((*iter)->first == StartID)
			{
				firstEdge = *iter;

				// push the first edge into the list
				sortedEdges.push_back(firstEdge);
				edges.remove(*iter);
				break;
			}
		}
		StartID += 1;
	}

	
	int lastsize = edges.size();
	vtkIdType tail = firstEdge->second, head = firstEdge->first;
	while (edges.size() != 0)
	{
		for (std::list<EdgePair*>::iterator iter = edges.begin(); iter != edges.end(); iter++)
		{
			// attach to front of the sorted list
			if ((*iter)->first == tail)
			{
				tail = (*iter)->second;
				sortedEdges.push_back((*iter));
				edges.remove((*iter));
				break;
			}

			// attach to tail of the sorted list
			if ((*iter)->second == head)
			{
				head = (*iter)->first;
				sortedEdges.insert(sortedEdges.begin(), (*iter));
				edges.remove((*iter));
				break;
			}

			
		}
		
		// Prevent endless loop
		if (edges.size() == lastsize)
		{
			bool BREAKER_FLAG = false;
			vtkWarningMacro("Input polyline is not continuous! Trying to re-order...");
			for (std::list<EdgePair*>::iterator iter = edges.begin(); iter != edges.end(); iter++)
			{
				EdgePair* l_pair = (*iter);
				if (l_pair->first == head)
				{
					l_pair->first = l_pair->second;
					l_pair->second = head;
					BREAKER_FLAG = true;
					break;
				}
				else if (l_pair->second == tail)
				{
					l_pair->second = l_pair->first;
					l_pair->first = tail;
					BREAKER_FLAG = true;
					break;
				}
			}
			// If re-order successful, redo outer loop.
			if (BREAKER_FLAG)
				break;


			vtkWarningMacro("Re-ordering failed. There are some original edges not added into the output.")
			cout << __FUNCDNAME__ << " Left off edges: ";
			for (std::list<EdgePair*>::iterator iter = edges.begin(); iter != edges.end(); iter++)
			{
				EdgePair* l_pair = (*iter);
				char msg[50];
				sprintf_s(msg, " [%i, %i] ", l_pair->first, l_pair->second);
				cout << msg;
			}
			cout << endl;
			cout << "Current Head and tail: " << head << " " << tail << endl;
			return 0;
		}
		else {
			lastsize = edges.size();
		}

	}

	return 1;
}


/*

Author:		Wong, Matthew Lun
Date:		2nd, Feb, 2016
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			Junior Research Assistant


This program is implemented to build a surface between two lines with different 
number of points and distrubution.


Wong Matthew Lun
Copyright (C) 2016	

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SURFACE_ENCLOSER_H
#define SURFACE_ENCLOSER_H

#include <algorithm>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCell.h>
#include <vtkIdList.h>
#include <vtkLine.h>
#include <vtkCellArray.h>
#include <vtkAppendPolyData.h>

// CalculateDistant
#include <vtkMath.h>

// CloseSurface
#include <vtkTriangle.h>
#include <vtkKdTree.h>
#include <vtkConnectivityFilter.h>
#include <vtkGeometryFilter.h>
#include <vtkSortDataArray.h>

// Willdel
#include <vtkXMLPolyDataWriter.h>

// restrurePD
#include <vtkCleanPolyData.h>
#include <vtkPointData.h>
#include <vtkConnectivityFilter.h>

// AlignLoops
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkCenterOfMass.h>


class SurfaceEncloser
{
public:
	SurfaceEncloser();
	~SurfaceEncloser();

	vtkPolyData* GetOutput();
	vtkPolyData* GetShiftedEdge();

	//void DirectionalConnectionOn();
	//void DirectionalConenctionOff();
	//void SetDirectionalConnection(bool);
	
	void SetInput(vtkPolyData*, vtkPolyData*);
	void SetEdgesAreLoops(bool);

	int Update();

private:
	int _AlignLoops();
	int _CloseSurface();
	int _FindNeighborSet(vtkIdType, vtkIdType, vtkIdList*, int, int, vtkIdType);
	void _FormCells(vtkIdType, vtkIdList*);

	// Utilities
	double _CalculateDistant(vtkIdList*);
	double* _FindNormal(vtkSmartPointer<vtkPolyData>, double*);
	void _GetCellList(vtkSmartPointer<vtkPolyData>, vtkIdList*);
	void _RestructurePD(vtkSmartPointer<vtkPolyData>, double[3]);
	void _VectorSub(double*, double*, double*);
	void _GetNeighborPoint(vtkSmartPointer<vtkPolyData>, vtkIdType, vtkIdList*);
	int _CheckNormalAligned();

	vtkSmartPointer<vtkPolyData> m_edge1;
	vtkSmartPointer<vtkPolyData> m_edge2;
	vtkSmartPointer<vtkPolyData> m_shiftedEdge2;
	vtkPolyData* m_appendedInput;
	vtkPolyData* m_outputPD;
	vtkIdList *m_edge1PtsInCells;
	vtkIdList *m_edge2PtsInCells;

	double	m_referenceCrossProduct[3];
	double* m_loopDirection;

	bool m_directionalConnection;
	bool LOOP_FLAG;

};
#endif // SURFACE_ENCLOSER_H


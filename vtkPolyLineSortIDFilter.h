/*
Author:		Wong, Matthew Lun
Date:		12, April 2017
Occupation:	Chinese University of Hong Kong,
			Department of Imaging and Inteventional Radiology,
			M.Phil Student

FileName: 	vtkPolyLineSortID.h

Descriptions:
	This class tries to sort a polyline into one with sequential IDs


Wong Matthew Lun
Copyright (C) 2017
*/
#ifndef VTK_POLYLINE_SORT_ID_FILTER_H
#define VTK_POLYLINE_SORT_ID_FILTER_H

#include <list>
#include "vtkPolyDataAlgorithm.h"
#include "vtkObjectFactory.h"
#include "vtkSetGet.h"

class vtkPolyLineSortDIFilter: public vtkPolyDataAlgorithm
{
public:
	static vtkPolyLineSortDIFilter* New();
	vtkTypeMacro(vtkPolyLineSortDIFilter, vtkPolyDataAlgorithm);

protected:
	vtkPolyLineSortDIFilter();
	~vtkPolyLineSortDIFilter();

	int RequestData(vtkInformation *vtkNotUsed(request),
		vtkInformationVector **inputVector,
		vtkInformationVector *outputVector);

	vtkIdType StartID;

private:
	typedef std::pair<vtkIdType, vtkIdType> EdgePair;

	std::list<EdgePair*> edges;
	std::list<EdgePair*> sortedEdges;

	int BuildSortedEdges();

};

#endif